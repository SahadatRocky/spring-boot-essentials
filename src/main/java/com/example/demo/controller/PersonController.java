package com.example.demo.controller;

import com.example.demo.model.Person;
import com.example.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/person")
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public List<Person> getAll() {
        return personService.getAllPeople();
    }


    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public Person getPerson(@NotNull  @PathVariable("id") UUID id){
        return personService.getPersonById(id)
                .orElse(null);
    }

    @PostMapping
    public UUID createNewPerson(@NotNull @Valid @RequestBody Person person){
        return personService.insertNewPerson(person);
    }

    @DeleteMapping("{id}")
    public void deletePerson(@NotNull @PathVariable("id") UUID id){
       personService.deletePerson(id);
    }

    @PutMapping("{id}")
   public void updatePerson(@NotNull @PathVariable("id") UUID id,
                            @NotNull @Valid @RequestBody Person person){
       personService.updatePerson(id, person);
   }



}
