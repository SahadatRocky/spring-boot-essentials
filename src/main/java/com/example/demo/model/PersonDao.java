package com.example.demo.model;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonDao {

    List<Person> getPeople();
    Optional<Person> getPerson(UUID id);
    int deletePerson(UUID id);
    int updatePerson(UUID id, Person person);

    UUID addPerson(UUID id, Person person);

    default UUID addPerson(Person person){
        UUID id = UUID.randomUUID();
        return addPerson(id,person);
    }
}
