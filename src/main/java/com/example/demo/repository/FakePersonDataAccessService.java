package com.example.demo.repository;

import com.example.demo.model.Person;
import com.example.demo.model.PersonDao;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDao")
public class FakePersonDataAccessService implements PersonDao {

    private final static List<Person> DB = new ArrayList<>();
    @Override
    public List<Person> getPeople() {
        return DB;
    }

    @Override
    public Optional<Person> getPerson(UUID id) {
        return  DB.stream().filter(person -> person.getId().equals(id)).findFirst();

    }

    @Override
    public int deletePerson(UUID id) {
        Optional<Person> personOptional = getPerson(id);
        if(!personOptional.isPresent()){
             return 0;
        }
        DB.remove(personOptional.get());
        return 1;
    }

    @Override
    public int updatePerson(UUID id, Person updatePerson) {
        return getPerson(id)
                .map(person1 -> {
                   int indexOfPersonToDelete = DB.indexOf(person1);
                   if(indexOfPersonToDelete>0){
                      DB.set(indexOfPersonToDelete,new Person(id,updatePerson.getName(),updatePerson.getAge()));
                      return 1;
                   }
                   return 0;
                }).orElse(0);
    }

    @Override
    public UUID addPerson(UUID id, Person person) {
        DB.add(new Person(id,person.getName(),person.getAge()));
        return id;
    }
}
